module.exports = {
  extends: [
    "plugin:@typescript-eslint/recommended",
    "plugin:unicorn/recommended",
    "plugin:vue/vue3-recommended",
    "prettier",
  ],
  env: {
    es2022: true,
  },
  parser: "vue-eslint-parser",
  parserOptions: {
    parser: "@typescript-eslint/parser",
    ecmaVersion: "latest",
    sourceType: "module",
  },
  plugins: ["unicorn", "@typescript-eslint"],
  reportUnusedDisableDirectives: true,
  overrides: [
    {
      files: ["*.vue"],
      rules: {
        "unicorn/filename-case": "off",
      },
    },
    {
      files: ["pages/**/*.vue", "app.vue", "layouts/*.vue"],
      rules: {
        "vue/multi-word-component-names": "off",
      },
    },
  ],
  rules: {
    "unicorn/filename-case": "off",
    "unicorn/prevent-abbreviations": "off",
    "unicorn/no-null": "off",
  },
}
