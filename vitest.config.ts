import { configDefaults, defineConfig } from "vitest/config"
import Vue from "@vitejs/plugin-vue"
export default defineConfig({
  plugins: [Vue()],

  esbuild: {
    tsconfigRaw: "{}",
  },

  test: {
    deps: {
      inline: ["date-fns"],
    },
    environment: "jsdom",
    exclude: [...configDefaults.exclude],
  },
})
