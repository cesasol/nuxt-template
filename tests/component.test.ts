import UserLoginForm from "../components/User/LoginForm.vue"
import { mount } from "@vue/test-utils"
import { describe, expect, it } from "vitest"
import { createDiscreteApi } from "naive-ui"

describe("Component Testing", async () => {
  const providerProps = {
    to: document.createElement("div"),
  }
  const { message } = createDiscreteApi(["message"], {
    messageProviderProps: providerProps,
  })
  expect(UserLoginForm).toBeTruthy()
  const messageProviderInjectionKey = Symbol("n-message-provider")
  const messageApiInjectionKey = Symbol("n-message-api")
  const wrapper = mount(UserLoginForm, {
    global: {
      provide: {
        "n-message-provider": {
          props: {},
          mergedClsPrefixRef: {},
        },
        [messageProviderInjectionKey]: {
          props: {},
          mergedClsPrefixRef: {},
        },
        [messageApiInjectionKey]: message,
        "n-message-api": message,
      },
    },
  })

  it("clicks a button", async () => {
    expect(wrapper.html()).toMatchSnapshot()

    await wrapper.get("button").trigger("click")

    expect(wrapper.text()).toContain("Validate")
  })
})
